Title: Auteur inconnu
Dates: 2021/09/16 20h00
       2021/09/17 20h00
       2021/09/18 18h00
       2021/09/20 13h30
       2021/09/21 13h30
       2021/09/21 20h00
       2021/09/22 20h00
Partner_logo: /images/img-20-21/equal-FR-LOGO-RGB__1_.png
Piece_author: Anaïs Moreau
Shape: trace
Sound: sonar
Event_type: Théâtre (report saison 20-21)
Shape_Status: active
Reservation_link:http://balsamine.billetterie.it/index.php/?mact=Agenda,cntnt01,DetailEvent,0&cntnt01id_event=228

De l'intime à l'extime : l'expérience du jeu de soi...
{: .phrase}

La nuit du 18 septembre 1998 - elle a 16 ans - elle se retrouve dans une chambre d'hôtel avec un jeune homme séduisant qu'elle vient de rencontrer, le soir-même, à la braderie du Canal Saint-Martin à Rennes. C'est sa "première fois". Et cela se passe mal. Elle découvre la sexualité sous la forme de la contrainte. Crûment, on appelle cela :viol. Un acte qu'elle garde, anonyme, pendant sept ans. Durant de nombreuses années, ses relations amoureuses sont comme des répétitions de cette première expérience : violences, névroses, rapports de domination et de soumission...

Dix-sept ans plus tard, elle porte plainte avec les souvenirs qui lui restent. Le dossier sera classé sans suite. Motif : auteur inconnu. C'est de cette expérience-là, réelle, brute, autobiographique, que Anaïs Moreau a voulu partir, pour l'ausculter, la partager et la porter vers une possible élucidation.
{: .push-down}

Avec 
:   Anaïs Moreau et Renaud Garnier-Fourniguet

Conception
:   Anaïs Moreau

Collaboration artistique 
:   Judith Ribardière

Création sonore 
:   Loup Mormont

Création lumière 
:   Nicolas Thill

Scénographie et costumes
:  Camille Collin

Aide à l'écriture et au jeu
:  Nicolas Luçon

Régie son
:   Antonin Simon

Stagiaire son
:   Némo Camus

Coproduction 
:   Théâtre la Balsamine

Soutiens 
:   Théâtre & Publics, de la Chaufferie, BAMP, Centre Culturel de la Vénerie, Zinneke ASBL,Agnès Limbos...

Ce projet est soutenu par equal.brussels de la Région de Bruxelles-Capitale.<a href="https://equal.brussels/fr">https://equal.brussels/fr</a>

Après une formation initiale en Bretagne ( Conservatoires de Rennes, Cie Dérézo à Brest et Cie la folle Pensée à Saint-Brieuc) , puis à l'ESACT(Liège) , la comédienne Anaïs Moreau a travaillé avec plusieurs metteurs en scène et artistes. Elle a joué notamment dans 1984 mis en scène par Mathias Simons, Revue Instantanée de Charlie Degotte, Enfant Zéro, et Eden expérience(s) mis en scène par Céline Ohrel, Les Misérables, spectacle de théâtre d’objets de la Compagnie Karyatides et actuellement (prochainement) Mange tes ronces, de la Cie Moquette production.
Au cinéma, elle a reçu le prix d’interprétation féminine au BSFF en 2013 dans « Raé », un court métrage d'Emmanuelle Nicot.
Parallèlement, elle a été assistante à la mise en scène d'Isabelle Gyselinck à l'ESACT, a mené des ateliers au Centre Culturel de la Vénerie avec Rémi Pons. Elle travaille ponctuellement à la Zinneke ASBL en tant que coordinatrice artistique auprès de personnes fragilisées psychologiquement et physiquement. En 2007, elle crée sa première mise en scène, Le Ka jetable, puis en 2013, Somniloquie Cacophonique.
{: .production }


-galerie-

![auteur inconnu](/images/img-21-22/auteur_inconnu_224_Balsamine_022821.jpg){: .image-process-large}
:   © Hichem Dahes

![auteur inconnu](/images/img-21-22/auteur_inconnu_234_Balsamine_022821.jpg){: .image-process-large}
:   © Hichem Dahes

![auteur inconnu](/images/img-21-22/auteur_inconnu_257_Balsamine_022821.jpg){: .image-process-large}
:   © Hichem Dahes

![auteur inconnu](/images/img-21-22/auteur_inconnu_277_Balsamine_022821.jpg){: .image-process-large}
:   © Hichem Dahes

![auteur inconnu](/images/img-21-22/auteur_inconnu_281_Balsamine_022821.jpg){: .image-process-large}
:   © Hichem Dahes

![auteur inconnu](/images/img-21-22/auteur_inconnu_318_Balsamine_022821.jpg){: .image-process-large}
:   © Hichem Dahes
